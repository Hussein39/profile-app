/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  compiler: {
    styledComponents: true
  },
  env: {
    API_ROUTE: process.env.API_ROUTE,
    LOCAL_ROUTE: process.env.LOCAL_ROUTE
  }
}

module.exports = nextConfig
