/**
 * Constants - COLOR
 */

 const COLOR = {
    black: '#32324D',
    danger: '#FF3333',
    primary: '#212748',
    primaryHover: 'rgb(123, 121, 255)',
    white: '#ffffff',
    spinning: '#5099f4',
    success: 'rgb(47, 104, 70)',
    bgButton: '#212748'
  }
  
  export default COLOR
  
  