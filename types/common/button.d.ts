/**
 * Types - Common - IButton
 */
 import { ReactNode } from 'react'
 import { ITheme } from './theme'
 
 export interface IButton {
   children: ReactNode
   centre: boolean
   context: string
   className: string
   disabled: boolean
   hoverContext: string
   icon: string
   iconHoverContext: string
   onClick: (event?: React.MouseEvent<HTMLButtonElement>) => void
   round: number
   title: string
   titleIcon: string
   textContext: string
   theme: ITheme
   type?: 'button' | 'submit' | 'reset'
 }
 
     