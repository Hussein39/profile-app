/**
 * Types - Common - IHeading
 */

 export interface IHeading {
    align?: string
    content?: string
    context?: string
    tag: string
  }
  
  