/**
 * Types - Common - IImage
 */

 export interface IImage {
    align?: string
    center?: boolean
    className?: string
    height?: number
    round?: string
    src: string
    alt: string
    width?: number
}
  
  