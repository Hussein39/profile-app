/**
 * Types - Common - IText
 */

 import { ReactNode } from 'react'

 export interface IText {
   align: string
   children: ReactNode
   content: string
   context: string
   className: string
   isInlineLabel: boolean
   size: number
   lineHeight: number
   label: string
   weight: string
 }
 
 