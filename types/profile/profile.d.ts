/**
 * Types - Profile - IProfile
 */

 export interface IProfile {
    name: string
    country: string
    image: string
  }
  