/**
 * Types - HomePage - IHomePage
 */

// Types
import { IProfile } from "../profile/profile";

 export interface IHomePage {
    onShowProfile: () => void;
    loading: boolean;
    profile: IProfile;
  }