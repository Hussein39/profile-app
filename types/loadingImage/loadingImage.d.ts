/**
 * Types - loadingImage - ILoadingImage
 */

 // Types
 import { IProfile } from "../profile/profile";

 export interface ILoadingImage {
    loading: boolean;
    isShow: boolean;
    profile: IProfile
  }
  