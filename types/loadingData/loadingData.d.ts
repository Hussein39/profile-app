/**
 * Types - loadingData - ILoadingData
 */

 export interface ILoadingData {
    loading: boolean;
    isShow: boolean;
    data: string;
  }
  