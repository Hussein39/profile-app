/**
 *  Components - Utils - Fetcher
 */

const fetcher = async function fetchAPI({ body, method, url }: any): Promise<any> {
  const res = await fetch(url, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    method,
    body
  })

  if (!res.ok) {
    return { error: true, status: res.status, message: 'Server Side Error!', data: {} }
  } else {
    const result = await res.text()
    const data = result === '' ? {} : JSON.parse(result) || []

    return { data, status: 200, error: false }
  }
}

export default fetcher

