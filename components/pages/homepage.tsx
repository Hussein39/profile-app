/**
 *  Components - Pages - Home Page
 */

// React
import { useState } from "react";

// UI
import Text from "../../components/atom/text";
import Button from "../atom/button";
import Heading from "../atom/heading";
import LoadingData from "../molecules/loadingData";
import LoadingImage from "../molecules/loadingImage";

// Style
import styled from "styled-components";

// Types
import { IHomePage } from "../../types/homePage/homePage";

const HomePage = ({
  loading,
  onShowProfile,
  profile,
}: IHomePage): JSX.Element => {
  const [isShow, setShow] = useState(true);

  const handleShowProfile = () => {
    setShow(!isShow);

    if (!isShow) {
      onShowProfile();
    }
  };

  return (
    <Wrapper>
      <Heading
        tag={"h1"}
        content="Profile Information"
        context="white"
        align="center"
      />
      <LoadingImage loading={loading} isShow={isShow} profile={profile} />
      <Text context="white" align="center" size={28} weight="bold">
        <LoadingData loading={loading} isShow={isShow} data={profile?.name} />
      </Text>
      <Text context="white" align="center" size={20} weight="bold">
        <LoadingData
          loading={loading}
          isShow={isShow}
          data={profile?.country}
        />
      </Text>
      <ButtonWrapper>
        <Button
          disabled={loading}
          context="primary"
          textContext="white"
          hoverContext="black"
          round={5}
          onClick={handleShowProfile}
          type="button"
        >
          {`${!isShow ? "Show" : "Hide"} Profile Info`}
        </Button>
      </ButtonWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 10px 38px;
  border-radius: 5px 5px;
  background: rgb(50, 60, 72);
  background: linear-gradient(
    180deg,
    rgba(50, 60, 72, 1) 0%,
    rgba(62, 73, 133, 1) 100%
  );

  box-shadow: -1px 10px 47px -5px rgba(0, 0, 0, 0.77);
  -webkit-box-shadow: -1px 10px 47px -5px rgba(0, 0, 0, 0.77);
  -moz-box-shadow: -1px 10px 47px -5px rgba(0, 0, 0, 0.77);

  @media (min-width: 576px) {
    width: 500px;
  }
`;

const ButtonWrapper = styled.h1`
  color: gray;
  display: flex;
  justify-content: center;
`;

export default HomePage;
