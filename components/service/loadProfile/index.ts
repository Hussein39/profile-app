/**
 *  Components - Service - Load Profile - Load
 */

// Utils
import fetcher from "../../utils/fetcher"
import profileAdapter from "../adapters/prfoileAdapter"

// Types
import { IProfile } from "../../../types/profile/profile"

const load = async (): Promise<IProfile> => {
  const load = await fetcher({ method: 'GET', url: `${process.env.LOCAL_ROUTE}/api/profile` })
  return profileAdapter(load);
}

export default load
