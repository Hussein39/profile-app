/**
 *  Components - Service - Adapter - ProfileAdapter
 */

// Types
import { IProfile } from "../../../types/profile/profile";

const profileAdapter = ({ data }: any): IProfile => {
    const { name, picture, location } = data || {};
    
    return {
        name: `${name?.title} ${name?.first} ${name?.last}`,
        image: picture?.large,
        country: location?.country
    }
    
}

export default profileAdapter