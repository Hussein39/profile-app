/**
 * Components - Molecules - LoadingImage
 */

// UI
import Spinning from "../../atom/spinning";
import Image from "../../atom/image";

// Style
import styled from "styled-components";

// Types
import { ILoadingImage } from "../../../types/loadingImage/loadingImage";

const LoadingImage = ({
  isShow,
  profile,
  loading,
}: ILoadingImage): JSX.Element => {
  return (
    <>
      {loading ? (
        <ImageWrapper>
          <Spinning />
          <Image
            center
            round="50%"
            align="center"
            width={100}
            height={100}
            src="/unknown.png"
            alt="unknown"
          />
        </ImageWrapper>
      ) : (
        <Image
          center
          round="50%"
          align="center"
          width={100}
          height={100}
          src={isShow && profile?.image ? profile?.image : "/unknown.png"}
          alt={isShow && profile?.image ? profile?.name : "-----"}
        />
      )}
    </>
  );
};

const ImageWrapper = styled.div`
  width: 100px;
  height: 100px;
  margin: 0px auto;
  position: relative;

  .spining {
    position: absolute;
    top: 24px;
    left: 38px;
  }
`;

export default LoadingImage;
