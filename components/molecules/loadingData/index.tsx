/**
 * Components - Molecules - LoadingData
 */

// UI
import Spinning from "../../atom/spinning";

// Types
import { ILoadingData } from "../../../types/loadingData/loadingData";

const LoadingData = ({ loading, isShow, data }: ILoadingData): JSX.Element => {
  if (isShow && !loading) {
    return <span>{data}</span>;
  } else if (isShow && loading) {
    return <Spinning />;
  } else {
    return <span> ---- </span>;
  }
};

export default LoadingData;
