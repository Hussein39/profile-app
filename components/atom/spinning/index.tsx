/**
 * Components - Atom - Spinning
 */

// Style
import styled from "styled-components";

const Spinning = (): JSX.Element => (
  <Wrapper className="spining">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </Wrapper>
);

const Wrapper = styled.div`
  display: inline-block;
  position: relative;
  width: 22px;
  height: 22px;

  div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 22px;
    height: 22px;
    margin: 4px;
    border: 3px solid ${({ theme }: any): any => theme?.COLOR.spinning};
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: ${({ theme }: any): any => theme?.COLOR.spinning} transparent
      transparent transparent;
  }
  div:nth-child(1) {
    animation-delay: -0.45s;
  }
  div:nth-child(2) {
    animation-delay: -0.3s;
  }
  div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
export default Spinning;
