/**
 * Components - Atom - Heading
 */

// React
import { createElement } from "react";

// Style
import styled, { css } from "styled-components";

// Types
import { IHeading } from "../../../types/common/heading";
import { CSSReturnType } from "../../../types/common/theme";

const Heading = ({ align, content, context, tag }: IHeading): JSX.Element => {
  return (
    <StyledHeading align={align} context={context} tag={tag}>
      {createElement(tag, { content, tag }, content)}
    </StyledHeading>
  );
};

const StyledHeading = styled.div<IHeading>`
  color: ${({ context, theme }): CSSReturnType =>
    context && theme.COLOR[context]};
  ${({ align, theme, tag }): CSSReturnType => css`
    font-family: ${theme.HEADING?.[tag]?.fontFamily};
    font-size: ${theme.HEADING?.[tag]?.fontSize};
    line-height: ${theme.HEADING?.[tag]?.lineHeight};
    text-transform: ${theme.HEADING?.[tag]?.textTransform};
    text-align: ${align};
    margin: 0;
  `}
`;

export default Heading;
