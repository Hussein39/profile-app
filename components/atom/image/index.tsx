/**
 * Components - Atom - Image
 */

// Style
import styled, { css } from "styled-components";

// Types
import { IImage } from "../../../types/common/image";
import { CSSReturnType } from "../../../types/common/theme";

const Image = ({
  alt,
  center,
  className,
  height,
  src,
  round,
  width,
}: IImage): JSX.Element => {
  return (
    <StyledHeading
      alt={alt}
      center={center}
      className={className}
      height={height}
      round={round}
      src={src}
      width={width}
    />
  );
};

const StyledHeading = styled.img<IImage>`
  ${({ round, center }): CSSReturnType => css`
    border-radius: ${round} ${round};
    margin: ${center && `0 auto`};
    display: ${center && `flex`};
  `}
`;

export default Image;
