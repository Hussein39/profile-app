/**
 * Main App
 */

// Next
import type { AppProps } from "next/app";

// Style
import "../styles/globals.css";
import { ThemeProvider } from "styled-components";
import theme from "../theme/theme";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}

export default MyApp;
