/**
 * Main Page
 */

// Next
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect, useState } from "react";
import styled from "styled-components";

// View
import HomePage from "../components/pages/homepage";
import load from "../components/service/loadProfile";

const Home: NextPage = () => {
  const [profile, setProfile] = useState({ name: "", image: "", country: "" });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    load().then((data) => {
      setProfile({ ...profile, ...data });
      setLoading(false);
    });
  }, []);

  const handleLoadProfile = (): void => {
    setLoading(true);

    load().then((data) => {
      setProfile({ ...profile, ...data });
      setLoading(false);
    });
  };

  return (
    <Wrapper>
      <Head>
        <title>Profile Information App</title>
        <meta name="description" content="Profile App" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomePage
        profile={profile}
        loading={loading}
        onShowProfile={handleLoadProfile}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Home;
