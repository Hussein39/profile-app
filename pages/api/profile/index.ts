/**
 * Pages - api - profile
 */

// Next
import { NextApiRequest, NextApiResponse } from "next"

// Utils
import fetcher from "../../../components/utils/fetcher"

export default async function  handler(_: NextApiRequest, res: NextApiResponse) {
  const result= await fetcher({url:process.env.API_ROUTE}) 
  res.status(result?.status).json(result?.data?.results?.[0] || {})
}