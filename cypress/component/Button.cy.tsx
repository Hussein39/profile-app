/**
 * Cypress - Component - Button
 */

// UI
import Button from '../../components/atom/button'
import theme from '../../theme/theme'

// Style
import { ThemeProvider } from 'styled-components'

describe('<Button>', () => {
  it('testing visibility', () => {
    cy.mount(<Button title="Button" />)
    cy.contains('Button').should('be.visible')
  })

  it('testing disabled button', () => {
    cy.mount(<Button title="Button" disabled />)
    cy.contains('Button').should('be.disabled')
  })

  it('testing children', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Button round={5} context="black" textContext="white">
          title
        </Button>
      </ThemeProvider>
    )
    cy.contains('title').should('be.visible')
  })
})
